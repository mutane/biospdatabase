
/****
//tabela provincia
@DataClassName("Provincia")
class Provincias extends Table{
  IntColumn get id => integer().autoIncrement().customConstraint('UNIQUE')();
  TextColumn get nome => text().withLength(max: 191)();
  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}

//tabela de bairros

class Bairros extends Table{
  IntColumn get id => integer().customConstraint('UNIQUE NOT NULL').autoIncrement()();
  TextColumn get nome => text().withLength(max: 191)();
  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();


//tabela de sexo
@DataClassName("Sexo")
class Sexos extends Table{
  IntColumn get id => integer().autoIncrement().customConstraint('UNIQUE')();
  TextColumn get nome => text().withLength(max: 191)();
  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}

//tabela de objectivos da visita
@DataClassName("ObjectivoDaVisita")
class ObjectivoDaVistas extends Table{
  IntColumn get id => integer().autoIncrement().customConstraint('UNIQUE')();
  TextColumn get nome => text().withLength(max: 191)();
  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}

//tabela de servicos encaminhados
@DataClassName("ServivoEncaminhado")
class ServicoEncaminhados extends Table{
  IntColumn get id => integer().autoIncrement().customConstraint('UNIQUE')();
  TextColumn get nome => text().withLength(max: 191)();
  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}

//tabela de documentos
@DataClassName("Documento")
class Documentos extends Table{
  IntColumn get id => integer().autoIncrement().customConstraint('UNIQUE')();
  TextColumn get nome => text().withLength(max: 191)();
  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}

//tabela de Motivos de abertura do processo
@DataClassName("MotivoDeAberturaDeProcesso")
class MotivoDeAberturaDeProcessos extends Table{
  IntColumn get id => integer().autoIncrement().customConstraint('UNIQUE')();
  TextColumn get nome => text().withLength(max: 191)();
  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}


//tabela de provenience
@DataClassName("Contacto")
class Contactos extends Table{
  IntColumn get id => integer().autoIncrement()();
  TextColumn get contacto => text().withLength(max: 25)();
  TextColumn get NomeDoResponsavel => text().withLength(max: 191)();
  IntColumn get  benificiarioId => integer().nullable().customConstraint('NULL REFERENCES benificiarios(id)')();
  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}

//tabela de bairros
@DataClassName("Benificiario")
class Benificiarios extends Table{
  final _uuid = Uuid();
  IntColumn get id => integer().autoIncrement().customConstraint('UNIQUE')();
  TextColumn get nome => text().withLength(max: 191)();
  TextColumn get ref => text().clientDefault(() => _uuid.v4()).withLength(max: 191).customConstraint("UNIQUE")();
  IntColumn get numeroDeVisita => integer()();
  DateTimeColumn get dataDeNascimento => dateTime()();
  DateTimeColumn get dataDeAtendimento => dateTime()();
  IntColumn get provinciaId => integer().nullable().customConstraint('NULL REFERENCES provincias(id)')();
  IntColumn get bairroId => integer().nullable().customConstraint('NULL REFERENCES bairros(id)')();
  IntColumn get sexoId => integer().nullable().customConstraint('NULL REFERENCES sexos(id)')();
  IntColumn get provinienciaId => integer().nullable().customConstraint('NULL REFERENCES proviniencias(id)')();
  IntColumn get ObjectivoDaVistaId => integer().nullable().customConstraint('NULL REFERENCES objectivo_da_vistas(id)')();

  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names s
  DateTimeColumn get updatedAat => dateTime()();
 }



 _________________________________________
|  _____________________________________  |
| |                                     | |
| |      NelsonMutane@Fenn.com          | |
| |_____________________________________| |
|_________________________________________|

 @DataClassName("BenificiarioEncaminhado")
class BenificiarioEncaminhados extends Table{
  IntColumn get  id => integer().autoIncrement()();
  IntColumn get  benificiarioId => integer().nullable().customConstraint('NULL REFERENCES benificiarios(id)')();
  TextColumn get objectivoDaVista => text().withLength(max: 191).nullable()();
  BoolColumn get necessitaDeAcompanhamenBairromiciliar => boolean().clientDefault(() => false)();
  DateTimeColumn get dataQueFoiRecebido => dateTime()();
  BoolColumn get problemaResolvido => boolean().clientDefault(() => false)();
  IntColumn get motivoDeAberturaDeProcesso =>  integer().nullable().customConstraint('NULL REFERENCES motivo_de_abertura_de_processos(id)')();

  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}

//tabela de benificiario servico
@DataClassName("BenificiarioEncaminhadoServicoEncaminhado")
class BenificiarioEncaminhadoServicoEncaminhados extends Table{
  IntColumn get id => integer().autoIncrement()();
  IntColumn get benificiarioEncaminhadoId => integer().nullable().customConstraint('NULL REFERENCES benificiario_encaminhados(id)')();
  IntColumn get servicoEncaminhadoId => integer().nullable().customConstraint('NULL REFERENCES servico_encaminhados(id)')();
  TextColumn get especificar => text().withLength(max: 191)();
  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}


////BenificiarioEncaminhadoDocumento
//tabela de BenificiarioEncaminhadoDocumento
@DataClassName("BenificiarioEncaminhadoDocumento")
class BenificiarioEncaminhadoDocumentos extends Table{
  IntColumn get id => integer().autoIncrement()();
  IntColumn get benificiarioEncaminhadoId => integer().nullable().customConstraint('NULL REFERENCES benificiario_encaminhados(id)')();
  IntColumn get documentoId => integer().nullable().customConstraint('NULL REFERENCES documentos(id)')();
  TextColumn get especificar => text().withLength(max: 191)();
  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}

//tabela de provenience
@DataClassName("Proviniencia")
class Proviniencias extends Table{
  IntColumn get id => integer().autoIncrement()();
  TextColumn get nome => text().withLength(max: 191)();
  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}

*/
}






//










//end generating tables database


LazyDatabase _openConection() {
  return LazyDatabase(() async {
    final dbFolder = await getApplicationDocumentsDirectory();
    final file = File(p.join(dbFolder.path,'hash.sqlite'));
    return VmDatabase(file);
  });
}

@UseMoor(tables:[Bairros,Contactos,BenificiarioEncaminhadoDocumentos,BenificiarioEncaminhadoServicoEncaminhados,Provincias,Sexos,ObjectivoDaVistas,ServicoEncaminhados,Documentos,MotivoDeAberturaDeProcessos,Benificiarios,BenificiarioEncaminhados,Proviniencias])
class AppDatabase extends _$AppDatabase{
  AppDatabase():super(_openConection());
  @override
  int get schemaVersion => 1;


  //list of all bairros
  Future<List<Bairro>> bairrosAll() => select(bairros).get();



    //real time list of all bairros
  Stream<List<Bairro>> watchbairros(){
    return (select(bairros)..orderBy([(t) => OrderingTerm(expression: t.nome,mode: OrderingMode.asc)])).watch();
  }

  Future addBairo(Bairro bairro) => into(bairros).insert(bairro);
  Future addProvincia(Provincia p) => into(provincias).insert(p);
    //create or update  bairros on database

  Future<bool> updateBairro(Bairro bairro) {
    return update(bairros).replace(bairro);
  }
  Future feelingLazy() {
    // delete the oldest nine tasks
    return (delete(bairros).go());
  }
  Future<Bairro> entryById(int id) {
    return (select(bairros)..where((t) => t.id.equals(id))).getSingle();
  }

}


  Future<List<Bairro>> get allBairroEntries => select(bairros).get();
  Stream<List<Bairro>> watchEntrie() {
    return (select(bairros)))).watch();
  }
  
  Future<List<Bairro>> sortEntriesAlphabetically() {
    return (select(bairros)).get();
  }
  Stream<Bairro> entryById(int id) {
    return (select(bairros)..where((t) => t.id.equals(id))).watchSingle();
  }

 
  Future updateA(Bairro entry) {
    return update(bairros).replace(entry);
  }
  Future feelingLazy() {
    // delete the oldest nine tasks
    return delete(bairros).go();
  }

  Future<int> addBairro(bairrosCompanion entry) {
    return into(bairros).insert(entry);
  }
