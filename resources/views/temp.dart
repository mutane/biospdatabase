  //get all Proviniencias
  Future<List<Proviniencia>> get allProvinienciaEntries => select(proviniencias).get();
  Stream<List<Proviniencia>> watchEntriesProviniencias() {
    return (select(proviniencias)).watch();
  }

  Future<List<Proviniencia>> allProvinienciasOrdered() {
    return (select(proviniencias)..orderBy([(t)=>OrderingTerm(expression: t.nome)])).get();
  }
  //get Proviniencia as id

  Stream<Proviniencia> watchProvinienciaById(int id) {
    return (select(proviniencias)..where((t) => t.id.equals(id))).watchSingle();
  }
  Future<Proviniencia> ProvinienciaById(int id) {
    return (select(proviniencias)..where((t) => t.id.equals(id))).getSingle();
  }
  //add Proviniencia
  Future<int> addProviniencia(Proviniencia entry) {
    return into(proviniencias).insert(entry);
  }
  Future<int> addProvinienciaCompanion(ProvinienciasCompanion entry) {
    return into(proviniencias).insert(entry);
  }

  //update Proviniencia
  Future updateProviniencia(Proviniencia entry) {
    return update(proviniencias).replace(entry);
  }
  Future updateProvinienciaCompanion(ProvinienciasCompanion entry) {
    return update(proviniencias).replace(entry);
  }

  //delete all Proviniencias
  Future deleteAllProviniencia() {
    // delete the oldest nine tasks
    return delete(proviniencias).go();
  }

  Future deleteProviniencia(int entry) {
    // delete the oldest nine tasks
    return (delete(proviniencias)..where((t) => t.id.equals(entry))).go();
  }