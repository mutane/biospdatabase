<?php

use Illuminate\Support\Facades\Route;

use App\Http\Resources\Benificiario as BenificiarioResource;
use App\Benificiario;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('developers','DevelopersController@index')->name('developers.index');

Route::get('/excel','ExportsController@export');
Route::get('/data', function () {
    return  BenificiarioResource::collection(Benificiario::paginate(1));
});

Route::get('/mpesa','MpesaController@index');