<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Resources\User as UserResource;
use App\User;

use App\Http\Resources\Benificiario as BenificiarioResource;
use App\Benificiario;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});




Route::middleware('auth:api')->get('/data', function () {
    return  BenificiarioResource::collection(Benificiario::all());
});


//resoures da api do aplicativo biospdatabase
Route::resource('bairro','BairroController')->middleware('auth:api');
Route::resource('provincias','ProvinciaController')->middleware('auth:api');
Route::resource('benificiario','BenificiarioController')->middleware('auth:api');
Route::resource('sexo','SexoController')->middleware('auth:api');
Route::resource('contacto','ContactoController')->middleware('auth:api');
Route::resource('objectivo','ObjectivoDaVisitaController')->middleware('auth:api');
Route::resource('b_encaminhado','BenificiarioEncaminhadoController')->middleware('auth:api');
Route::resource('documento','DocumentoController')->middleware('auth:api');
Route::resource('proviniencias','ProvinienciaController')->middleware('auth:api');
Route::resource('servico','ServicoEncaminhadoController')->middleware('auth:api');
Route::resource('motivo','MotivoDeAberturaDeProcessoController')->middleware('auth:api');
//resouce da biblioteca de sync
