<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Documento extends Model
{
    protected $fillable = [
        'nome',
    ];

    public function benificiarioEncaminhados()
    {
        return $this->belongsToMany('App\BenificiarioEncaminhado')->withPivot(['id','benificiario_encaminhado_id','documento_id'])->withTimestamps();
    }
}
