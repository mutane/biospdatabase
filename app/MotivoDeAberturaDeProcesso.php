<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MotivoDeAberturaDeProcesso extends Model
{
    protected $fillable = [
        'nome',
    ];

    public function benificiariosEncaminhados()
    {
        return $this->belongsTo('App\BenificiarioEncaminhado');
    }
}
