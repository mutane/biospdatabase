<?php

namespace App\Exports;

use App\User;
use App\Benificiario;
use App\Http\Resources\Benificiario as ExportB;
use Maatwebsite\Excel\Concerns\FromCollection;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\BeforeWriting;
use Maatwebsite\Excel\Sheet;
use \Maatwebsite\Excel\Writer;

Writer::macro('setCreator', function (Writer $writer, string $creator) {
    $writer->getDelegate()->getProperties()->setCreator($creator);
});
Sheet::macro('setOrientation', function (Sheet $sheet, $orientation) {
    $sheet->getDelegate()->getPageSetup()->setOrientation($orientation);
});

Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
    $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
});

class userExport implements WithMultipleSheets, WithEvents
{
     
    use Exportable;

    protected $year;
    
    public function __construct(int $year)
    {
        $this->year = $year;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];
        $title = ['SA'];

        for ($var = 0; $var < count($title); $var++) {

           // dd($title[$var]);
            $sheets[] = new InvoicesPerMonthSheet($title[$var]);
        }

        return $sheets;
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            BeforeExport::class => function(BeforeExport $event) {
                $event->writer->getProperties()->setCreator('Nelson Mutane')->setLastModifiedBy("Nelson Mutane")->setSubject("Biosp Database")->setKeywords("BD Biosp")->setCategory("Aplicativo");
                $event->writer->getProperties()->setTitle("Relatório Mensal")->setDescription("Relatório de dados do aplicativo biosp database que e gerado mensalmente.");
               

            },
            BeforeWriting::class => function (BeforeWriting $event)
            {
               
                //dd($sheet);
            },
            AfterSheet::class => function(AfterSheet $event) {
                //$event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);

                
            },
        ];
    }


   
}



use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithStyles;
use App\Http\Resources\Benificiario as ResourceBen;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;


class InvoicesPerMonthSheet implements FromCollection, WithTitle,WithHeadings,ShouldAutoSize,WithStyles,WithColumnWidths,WithColumnFormatting
{
    private $month;
    private $year;
  

    public function __construct(string $year)
    {
        //$this->$title = $title;
        $this->year = $year;
       // dd($this->$title);
        
    }



     public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('A3:U3')->getFont()->setBold(true);
         $sheet->mergeCells("A1:u2");
                $sheet->getStyle('A1:U2')->getFill()->applyFromArray(
                     [
                          'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                          'rotation' => 0,
                          'startColor' => [
                              'argb' => '366092'
                          ],
                          'endColor' => [
                              'argb' => '366092'
                          ]
                      ],   
                  );
                $sheet->getStyle('A3:U3')->getFont()->getColor()->applyFromArray(['rgb' => 'FFFFFF']);
                $sheet->getStyle('A1:U2')->getFont()->getColor()->applyFromArray(['rgb' => 'FFFFFF']);
                $sheet->getStyle('A3:U3')->getFill()->applyFromArray(
                     [
                          'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                          'rotation' => 0,
                          'startColor' => [
                              'argb' => '16365c'
                          ],
                          'endColor' => [
                              'argb' => '16365c'
                          ]
                      ],
                );
                $sheet->getStyle('A4:A'.(count(Benificiario::all())+500))->getFill()->applyFromArray(
                     [
                          'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                          'rotation' => 0,
                          'startColor' => [
                              'argb' => 'e6e6e6'
                          ],
                          'endColor' => [
                              'argb' => 'e6e6e6'
                          ]
                      ],
                );


              $sheet->getStyle('A1:u2')->getAlignment()->applyFromArray(
                     [
                         'horizontal'   => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                         'vertical'     => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                         'textRotation' => 0,
                         'wrapText'     => TRUE,
                         'indent'=>4
                     ]
              );
               $sheet->getStyle('A3:u3')->getAlignment()->applyFromArray(
                     [
                         'horizontal'   => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                         'vertical'     => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                         'textRotation' => 0,
                         'wrapText'     => TRUE,
                     ]
              );
               $sheet->getStyle('A3:u3')->getFont()->applyFromArray(
                      [
                          'name' => 'Calibri',
                          'bold' => TRUE,
                          'italic' => FALSE,
                          //'underline' => \PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_DOUBLE,
                          'strikethrough' => FALSE,
                          'color' => [
                              'rgb' => 'ffffff'
                          ],
                          'size' =>8
                      ]
                  );
               $sheet->getStyle('A4:A'.(count(Benificiario::all())+500))->getBorders()->applyFromArray(
                          [
                              'allBorders' => [
                                  'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                  'color' => [
                                      'rgb' => 'A5A5A5'
                                  ]
                              ]
                          ]
                  );
               $sheet->getStyle('A3:U3')->getBorders()->applyFromArray(
                          [
                              'allBorders' => [
                                  'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                  'color' => [
                                      'rgb' => '366091'
                                  ]
                              ]
                          ]
                  );
    }


    /**
     * @return Builder
     */
    public function collection()
    {
        return ResourceBen::collection(\App\Benificiario::all());
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->year;
    }

    public function headings(): array
    {
       
        return [
            ['BASE DE DADOS DE REGISTO DO BENEFICIARIOS - BIOSP'],
            [' '],
            ['Id','Nome Completo','1° Visita ou Frequência','Proveniência','Província','Bairro','Sexo','Data de Nascimento','Contactos','Data de atendimento','Objetivo da visita','Motivo de abertura do processo','Documentos necessários','Serviço Encaminhado','Especificar Serviço Encaminhado','Necessita de acompanhamento domiciliar?','Data em que foi recebida pelo serviço','Problema resolvido?','Referencia','Data de lançamento do dado','Data da atualização do dado']];
    }

    public function columnWidths(): array
    {
        return [
            'A' => 10, 
            'b' => 25,          
            'c' => 15, 
            'd' => 25, 
            'e' => 25, 
            'f' => 25, 
            'g' => 20, 
            'h' => 25, 
            'i' => 25, 
            'j' => 25,
            'k' => 25, 
            'l' => 25, 
            'm' => 25,  
            'n' => 25, 
            'o' => 25, 
            'p' => 25, 
            'q' => 13,
            'u' => 25,  
        ];
    }
 public function columnFormats(): array
    {
        return [
            'H' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'J' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'T' => NumberFormat::FORMAT_DATE_DATETIME,
            'U' => NumberFormat::FORMAT_DATE_DATETIME,
        ];
    }
   
}