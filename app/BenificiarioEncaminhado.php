<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BenificiarioEncaminhado extends Model
{
    protected $fillable = [
        'objecto_da_visita','data_que_foi_recebido','problema_resolvido','benificiario_id','motivo_de_abertura_de_processo_id'
    ];
    public function benificiario()
    {
        return $this->belongsTo('App\Benificiario');
    }
     public function motivoDeAberturaDeProcesso()
    {
        return $this->belongsTo('App\MotivoDeAberturaDeProcesso');
    }

    public function servicoEncaminhados()
    {
        return $this->belongsToMany('App\ServicoEncaminhado')->withPivot(['id','benificiario_encaminhado_id','servico_encaminhado_id','especificar'])->withTimestamps();
    }

//->withPivot(['id','benificiario_encaminhado_id','servico_encaminhado_id','especificar'])->withTimestamps()

     public function documentos()
    {
        return $this->belongsToMany('App\Documento');
    }
}
  