<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServicoEncaminhado extends Model
{
   protected $fillable = [
        'nome',
    ];

     public function benificiarioEncaminhados()
    {
        return $this->belongsToMany('App\BenificiarioEncaminhado')->withPivot(['id','benificiario_encaminhado_id','servico_encaminhado_id','especificar'])->withTimestamps();
    }

}
