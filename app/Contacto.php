<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacto extends Model
{
    protected $fillable = [
        'contacto','nome_do_responsavel','benificiario_id'
    ];
     public function benificiario()
    {
        return $this->belongsTo('App\Benificiario');
    }
}
