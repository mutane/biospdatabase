<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bairro extends Model
{
    protected $fillable = [
        'nome',
    ];

    public function benificiarios()
    {
        return $this->hasMany('App\Benificiario');
    }
}
