<?php
namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;



class Benificiario extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       // dd(date($this->created_at));

        $bool = $boolN = "Não";

        if ($this->benificiarioEncaminhado['problema_resolvido']==1) {
           $bool = "Sim";
        }

        if ($this->necessita_de_acompanhamento_domicialiar==1) {
           $boolN = "Sim";
        }

        $documentos = $servicos = $especificar = $contactos= array();

        if($this->benificiarioEncaminhado['documentos']!=null){
             $documentos = $this->benificiarioEncaminhado['documentos']->pluck('nome');
             for ($i=0; $i <count($documentos) ; $i++) { 
                if ($i == 0) {
                    if ($documentos[$i]!=null) {
                       $aux = $documentos[$i];
                    }
                    
                }else{
                     if ($documentos[$i]!=null) {
                       $aux .=", ".$documentos[$i];
                    }
                    
                }
            }
            $documentos = $aux;
        }

        if ($this->benificiarioEncaminhado['servicoEncaminhados']!=null) {
            $servicos = $this->benificiarioEncaminhado['servicoEncaminhados']->pluck('nome');
            $especificar = $this->benificiarioEncaminhado['servicoEncaminhados']->pluck('especificar');
            $aux = $aux2 = "";

            for ($i=0; $i <count($servicos) ; $i++) { 
                if ($i == 0) {
                    if ($servicos[$i]!=null) {
                       $aux = $servicos[$i];
                    }
                    
                }else{
                     if ($servicos[$i]!=null) {
                       $aux .=", ".$servicos[$i];
                    }
                    
                }
            }

            $servicos = $aux;

            for ($i=0; $i <count($especificar) ; $i++) { 

                if ($i == 0) {
                    if ($especificar[$i]!=null) {
                       $aux2 = $especificar[$i];
                    }
                    
                }else{
                    if ($especificar[$i]!=null) {
                        $aux2 .=", ".$especificar[$i];
                    }
                    
                }
            }

            $especificar = $aux2;
        }

        if ($this->contactos!=null) {
            $contactos = $this->contactos->pluck('contacto');
            $aux = "";

            for ($i=0; $i <count($contactos) ; $i++) { 
                if ($i == 0) {
                    if ($contactos[$i]!=null) {
                       $aux = $contactos[$i];
                    }
                    
                }else{
                     if ($contactos[$i]!=null) {
                       $aux .=", ".$contactos[$i];
                    }
                    
                }
            }
            $contactos = $aux;
        }


        return [
            'ID' => $this->id,
            'Nome' => $this->nome,
            'Numero de Frequência'=>$this->numero_de_visita,
            'Proveniência'=>$this->proviniencia['nome'],
            'Província' => $this->provincia['nome'],
            'Bairro' =>$this->bairro['nome'],
            'Sexo' => $this->sexo['nome'],
            'Data de nascimento'=>$this->data_de_nascimento,
            'Contactos' =>$contactos,
            'Data de atendimento'=>$this->data_de_atendimento, 
            'Objetivo da visita' =>$this->objectivo_da_visita['nome'],
            'Motivo de abertura do processo'=>$this->benificiarioEncaminhado['motivoDeAberturaDeProcesso']['nome'],
            'Documentos necessários'=>$documentos,
            'Serviço Encaminhado' =>$servicos,
            'Especificar Serviço Encaminhado' =>$especificar,
            'Necessita de acompanhamento domiciliar?'=>$boolN,
            'Data em que foi recebido pelo serviço' => $this->benificiarioEncaminhado['data_que_foi_recebido'],
            'Problema resolvido?' => $bool,
            'Referencia' =>$this->ref,
            'Data de lançamento do dado' =>date($this->created_at),
            'Data da atualização do dado' =>date($this->updated_at), 

        ];
   
    }
}
/*
id: 6,
     nome: "Mr. Gavin Goldner",
     ref: "1a5ad25d-4e04-36f6-86ad-fae09e57e860",
     numero_de_visita: 8,
     data_de_nascimento: "1970-01-01",
     data_de_atendimento: "2020-09-01",
     provincia_id: 0,
     bairro_id: 7,
     sexo_id: 7,
     objectivo_da_visita_id: 4,
     created_at: "2020-09-01 21:30:36",
     updated_at: "2020-09-01 21:30:36",
?*/