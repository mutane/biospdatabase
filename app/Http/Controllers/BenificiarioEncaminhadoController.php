<?php

namespace App\Http\Controllers;

use App\BenificiarioEncaminhado as Ben;
use Illuminate\Http\Request;

class BenificiarioEncaminhadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Ben::get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validator = $request->validate([
                  'objecto_da_visita'=>'required',
                  'necessita_de_acompanhamento_domicialiar'=>'required',
                  'data_que_foi_recebido'=>'required',
                  'problema_resolvido'=>'required',
                  'benificiario_id'=>'required',
                  'motivo_de_abertura_de_processo_id'=>'required'
              ]);
         if ($validator) {
            try {
                $benificiario = Ben::create($request->all());
                Ben::find($benificiario->id)->documentos()->attach($request['documentos']);
                Ben::find($benificiario->id)->servicoEncaminhados()->attach($request['servico']);
                return  $benificiario;

            } catch (Exception $e) {
                return $e;
            }
             
         }else{
            return $validator;
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BenificiarioEncaminhado  $benificiarioEncaminhado
     * @return \Illuminate\Http\Response
     */
    public function show(Ben $b_encaminhado)
    {
        return $b_encaminhado;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BenificiarioEncaminhado  $benificiarioEncaminhado
     * @return \Illuminate\Http\Response
     */
    public function edit(Ben $b_encaminhado)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BenificiarioEncaminhado  $benificiarioEncaminhado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ben $b_encaminhado)
    {
       $validator = $request->validate([
                  'objecto_da_visita'=>'required',
                  'necessita_de_acompanhamento_domicialiar'=>'required',
                  'data_que_foi_recebido'=>'required',
                  'problema_resolvido'=>'required',
                  'benificiario_id'=>'required',
                  'motivo_de_abertura_de_processo_id'=>'required'
              ]);
         if ($validator) {
            try {
                $benificiario = Ben::create($request->all());
                Ben::find($benificiario->id)->documentos()->attach($request['documentos']);
                Ben::find($benificiario->id)->servicoEncaminhados()->attach($request['servico']);
                return  $benificiario;

            } catch (Exception $e) {
                return $e;
            }
             
         }else{
            return $validator;
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BenificiarioEncaminhado  $benificiarioEncaminhado
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ben $b_encaminhado)
    {
        $benificiario = $b_encaminhado;
         try {
            if (Ben::find($benificiario->id)!=null) {
                Ben::find($benificiario->id)->servicoEncaminhados()->detach(); 
                Ben::find($benificiario->id)->documentos()->detach();
            }
            $result = $benificiario->delete();
            
            if ($result) {
                 return response()->json(["message"=>"Beneficiário encaminhado removido com sucesso"]);
            }else{
                 return response()->json(["message"=>"Erro ao remover beneficiário encaminhado"]);
            }
           
            
        } catch (Exception $e) {
            return $e;
        }
    }
}
