<?php

namespace App\Http\Controllers;

use App\Contacto;
use Illuminate\Http\Request;

class ContactoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Contacto::get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
                  'contacto'=>'required',
                  'nome_do_responsavel'=>'required',
                  'benificiario_id'=>'required'
              ]);
        if ($validator) {
            try {
                $result = $contacto->create($request->all());

                if ($result) {
                    return $contacto;
                }else{
                    return response()->json(["message"=>"Erro ao inserir contacto"]);
                }
            } catch (Exception $e) {
                return $e;
            }
        }else{
            return $validator;
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contacto  $contacto
     * @return \Illuminate\Http\Response
     */
    public function show(Contacto $contacto)
    {
        return $contacto;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contacto  $contacto
     * @return \Illuminate\Http\Response
     */
    public function edit(Contacto $contacto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contacto  $contacto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contacto $contacto)
    {
        $validator = $request->validate([
                  'contacto'=>'required',
                  'nome_do_responsavel'=>'required',
                  'benificiario_id'=>'required'
              ]);

            if ($validator) {
                try {
                        $result = $contacto->update($request->all());

                        if ($result) {
                            return $contacto;
                        }else{
                            return response()->json(["message"=>"Erro ao atualizar contacto"]);
                        }
                    } catch (Exception $e) {
                        return $e;
                    }
            }else{

                return $validator;
            }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contacto  $contacto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contacto $contacto)
    {
        try {
            Contacto::find($contacto->id)->benificiario()->detach();
            $result = $contacto->delete();
            if ($result) {
                return response()->json(["message"=>"Contacto removido com sucesso"]);
            }else{
                return response()->json(["message"=>"Erro ao remover contacto"]);
            }
        } catch (Exception $e) {
            return $e;
        }
    }
}
