<?php

namespace App\Http\Controllers;

use App\Proviniencia;
use Illuminate\Http\Request;

class ProvinienciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Proviniencia::get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
                  'nome'=>'required|max:191',
              ]);

         if ($validator) {
             
            try {
                return Proviniencia::create($request->all());
                
            } catch (Exception $e) {
                return $e;
            }
             
         }else{
            return $validator;
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Proviniencia  $proviniencia
     * @return \Illuminate\Http\Response
     */
    public function show(Proviniencia $proviniencia)
    {
        return $proviniencia;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Proviniencia  $proviniencia
     * @return \Illuminate\Http\Response
     */
    public function edit(Proviniencia $proviniencia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Proviniencia  $proviniencia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Proviniencia $proviniencia)
    {
         
         $validator = $request->validate([
                  'nome'=>'required|max:191',
              ]);

         if ($validator) {
             
            try {
                $result =  $proviniencia->update($request->all());
                if ($result) {
                    return $proviniencia;
                }else{
                    return response()->json(["message"=>"Erro ao atualizar proveniência"]);
                }
                
            } catch (Exception $e) {
                return $e;
            }
             
         }else{
            return $validator;
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Proviniencia  $proviniencia
     * @return \Illuminate\Http\Response
     */
    public function destroy(Proviniencia $proviniencia)
    {
       try {
              $result =  $proviniencia->delete();
             if ($result) {
                return response()->json(["message"=>"Proveniência removida com sucesso"]);
             }else{
                return response()->json(["message"=>'Erro ao remover proveniência']);
             }
            
        } catch (Exception $e) {
            return $e;
        }
        
    }
}
