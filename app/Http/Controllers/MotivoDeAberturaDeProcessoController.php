<?php

namespace App\Http\Controllers;

use App\MotivoDeAberturaDeProcesso as Motivo;
use Illuminate\Http\Request;

class MotivoDeAberturaDeProcessoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Motivo::get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
                  'nome'=>'required|max:191',
              ]);

         if ($validator) {
             
            try {
                return Motivo::create($request->all());
                
            } catch (Exception $e) {
                return $e;
            }
             
         }else{
            return $validator;
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MotivoDeAberturaDeProcesso  $motivoDeAberturaDeProcesso
     * @return \Illuminate\Http\Response
     */
    public function show(Motivo $motivo)
    {
        return $motivo;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MotivoDeAberturaDeProcesso  $motivoDeAberturaDeProcesso
     * @return \Illuminate\Http\Response
     */
    public function edit(Motivo $motivo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MotivoDeAberturaDeProcesso  $motivoDeAberturaDeProcesso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Motivo $motivo)
    {
         $validator = $request->validate([
                  'nome'=>'required|max:191',
              ]);

         if ($validator) {
             
            try {
                $result =  $motivo->update($request->all());
                if ($result) {
                    return $motivo;
                }else{
                    return response()->jsoon(['message'=>'Erro ao atualizar o Motivo de abertura de processo']);
                }
                
            } catch (Exception $e) {
                return $e;
            }
             
         }else{
            return $validator;
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MotivoDeAberturaDeProcesso  $motivoDeAberturaDeProcesso
     * @return \Illuminate\Http\Response
     */
    public function destroy(Motivo $motivo)
    {
        try {
              $result  = $motivo->delete();
             if ($result) {
                return response()->json(["message"=>"Motivo removido com sucesso"]);
             }else{
                return response()->json(["message"=>'Erro ao remover motivo']);
             }
            
        } catch (Exception $e) {
            return $e;
        }
    }
}
