<?php

namespace App\Http\Controllers;

use App\Sexo;
use Illuminate\Http\Request;

class SexoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Sexo::get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
                  'nome'=>'required|max:191',
              ]);

         if ($validator) {
             
            try {
                return Sexo::create($request->all());
                
            } catch (Exception $e) {
                return $e;
            }
             
         }else{
            return $validator;
         }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sexo  $sexo
     * @return \Illuminate\Http\Response
     */
    public function show(Sexo $sexo)
    {
        return $sexo;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sexo  $sexo
     * @return \Illuminate\Http\Response
     */
    public function edit(Sexo $sexo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sexo  $sexo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sexo $sexo)
    {
        $validator = $request->validate([
                  'nome'=>'required|max:191',
              ]);

         if ($validator) {
             
            try {
                $result  = $sexo->update($request->all());

                if ($result) {
                    return $sexo;
                }else{

                    return response()->json(["message"=>'Erro ao atualizar sexo']);
            
                }
                
            } catch (Exception $e) {
                return $e;
            }
             
         }else{
            return $validator;
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sexo  $sexo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sexo $sexo)
    {
        try {
              $result =  $sexo->delete();
             if ($result) {
                return response()->json(["message"=>"sexo removida com sucesso"]);
             }else{
                return response()->json(["message"=>'Erro ao remover sexo']);
             }
            
        } catch (Exception $e) {
            return $e;
        }
        
    }
}

