<?php

namespace App\Http\Controllers;

use App\ObjectivoDaVisita;
use Illuminate\Http\Request;

class ObjectivoDaVisitaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ObjectivoDaVisita::get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
                  'nome'=>'required|max:191',
              ]);

         if ($validator) {
             
            try {
                return ObjectivoDaVisita::create($request->all());
                
            } catch (Exception $e) {
                return $e;
            }
             
         }else{
            return $validator;
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ObjectivoDaVisita  $objectivoDaVisita
     * @return \Illuminate\Http\Response
     */
    public function show(ObjectivoDaVisita $objectivo)
    {
        return $objectivo;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ObjectivoDaVisita  $objectivoDaVisita
     * @return \Illuminate\Http\Response
     */
    public function edit(ObjectivoDaVisita $objectivo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ObjectivoDaVisita  $objectivoDaVisita
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ObjectivoDaVisita $objectivo)
    {
        $validator = $request->validate([
                  'nome'=>'required|max:191',
              ]);

         if ($validator) {
             
            try {
                $result  = $objectivo->update($request->all());

                if ($result) {
                    return $objectivo;
                }else{

                    return response()->json(["message"=>'Erro ao atualizar objetivo da visita.']);
            
                }
                
            } catch (Exception $e) {
                return $e;
            }
             
         }else{
            return $validator;
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ObjectivoDaVisita  $objectivoDaVisita
     * @return \Illuminate\Http\Response
     */
    public function destroy(ObjectivoDaVisita $objectivo)
    {
        try {
              $result =  $objectivo->delete();
             if ($result) {
                return response()->json(["message"=>"Objetivo da visita removida com sucesso."]);
             }else{
                return response()->json(["message"=>'Erro ao remover objetivo da visita.']);
             }
            
        } catch (Exception $e) {
            return $e;
        }
        
    }
}
