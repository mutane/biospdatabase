<?php

namespace App\Http\Controllers;

use App\ServicoEncaminhado as Servico;
use Illuminate\Http\Request;

class ServicoEncaminhadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Servico::get(); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
         $validator = $request->validate([
                  'nome'=>'required|max:191',
              ]);

         if ($validator) {
             
            try {
                return Servico::create($request->all());
                
            } catch (Exception $e) {
                return $e;
            }
             
         }else{
            return $validator;
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ServicoEncaminhado  $servicoEncaminhado
     * @return \Illuminate\Http\Response
     */
    public function show(Servico $servico)
    {
        return $servico;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ServicoEncaminhado  $servicoEncaminhado
     * @return \Illuminate\Http\Response
     */
    public function edit(Servico $servicoEncaminhado)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ServicoEncaminhado  $servicoEncaminhado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Servico $servico)
    {
         $validator = $request->validate([
                  'nome'=>'required|max:191',
              ]);

         if ($validator) {
             
            try {
                $result = $servico->update($request->all());

                if ($result) {
                    return $servico;
                }else{
                    return response()->json(["message"=>'Erro ao atualizar serviço encaminhado']);
                }
                
            } catch (Exception $e) {
                return $e;
            }
             
         }else{
            return $validator;
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServicoEncaminhado  $servicoEncaminhado
     * @return \Illuminate\Http\Response
     */
    public function destroy(Servico $servico)
    {
        try {
             $result = $servico->delete();
             if ($result) {
                return response()->json(['message'=>"Serviço encaminhado removido com sucesso"]);
             }else{
                return response()->json(['message'=>"Erro removendo o serviço encaminhado"]);
             }
        } catch (Exception $e) {
            return $e;
        }
    }
}
