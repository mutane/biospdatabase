<?php

namespace App\Http\Controllers;

use App\Provincia;
use Illuminate\Http\Request;

class ProvinciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Provincia::get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validator = $request->validate([
                  'nome'=>'required|max:191',
              ]);

         if ($validator) {
             
            try {
                return Provincia::create($request->all());
                
            } catch (Exception $e) {
                return $e;
            }
             
         }else{
            return $validator;
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Provincia  $provincia
     * @return \Illuminate\Http\Response
     */
    public function show(Provincia $provincia)
    {

        return $provincia;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Provincia  $provincia
     * @return \Illuminate\Http\Response
     */
    public function edit(Provincia $provincia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Provincia  $provincia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Provincia $provincia)
    {
          
         $validator = $request->validate([
                  'nome'=>'required|max:191',
              ]);

         if ($validator) {
             
            try {
                $result  = $provincia->update($request->all());

                if ($result) {
                    return $provincia;
                }else{

                    return response()->json(["message"=>'Erro ao atualizar província']);
            
                }
                
            } catch (Exception $e) {
                return $e;
            }
             
         }else{
            return $validator;
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provincia  $provincia
     * @return \Illuminate\Http\Response
     */
    public function destroy(Provincia $provincia)
    {
        try {
              $result =  $provincia->delete();
             if ($result) {
                return response()->json(["message"=>"Província removida com sucesso"]);
             }else{
                return response()->json(["message"=>'Erro ao remover província']);
             }
            
        } catch (Exception $e) {
            return $e;
        }
        
    }
}
