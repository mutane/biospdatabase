<?php

namespace App\Http\Controllers;

use App\Documento;
use Illuminate\Http\Request;

class DocumentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Documento::get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
                  'nome'=>'required|max:191',
              ]);

         if ($validator) {
             
            try {
                return Documento::create($request->all());
                
            } catch (Exception $e) {
                return $e;
            }
             
         }else{
            return $validator;
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Documento  $documento
     * @return \Illuminate\Http\Response
     */
    public function show(Documento $documento)
    {
        return $documento;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Documento  $documento
     * @return \Illuminate\Http\Response
     */
    public function edit(Documento $documento)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Documento  $documento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Documento $documento)
    {
        $validator = $request->validate([
                  'nome'=>'required|max:191',
              ]);

         if ($validator) {
             
            try {
                $result = $documento->update($request->all());

                if ($result) {
                    return $documento;
                }else{
                    return response()->json(["message"=>'Erro ao atualizar documento']);
                }
                
            } catch (Exception $e) {
                return $e;
            }
             
         }else{
            return $validator;
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Documento  $documento
     * @return \Illuminate\Http\Response
     */
    public function destroy(Documento $documento)
    {
        try {
             $result = $documento->delete();
             if ($result) {
                return response()->json(['message'=>"Documento removido com sucesso"]);
             }else{
                return response()->json(['message'=>"Erro removendo o documento"]);
             }
        } catch (Exception $e) {
            return $e;
        }
    }
}
