<?php

namespace App\Http\Controllers;

use App\Bairro;
use Illuminate\Http\Request;

/**
 * @group  Bairro
 *
 * Este e' o controlador responsável pelo crud dos bairros
 */

class BairroController extends Controller
{
    /**
     * Função de imprimir toda lista de bairros da aplicação

     *  [Esta função não recebe nenhum parâmetro]
     *  [dominio/api/benificiario@index]
     * 
     * @response  [
            {
                "id": 1,
                "nome": "Chipangara",
                "created_at": "2020-09-08T18:44:32.000000Z",
                "updated_at": "2020-09-08T18:44:32.000000Z"
            },
            {
                "id": 2,
                "nome": "Mananga",
                "created_at": "2020-09-08T18:44:44.000000Z",
                "updated_at": "2020-09-08T18:44:44.000000Z"
            },
            {
                "id": 16,
                "nome": "Nickolas Jacobi",
                "created_at": "2020-09-08T21:17:14.000000Z",
                "updated_at": "2020-09-08T21:17:14.000000Z"
            }
        ]
    * @queryParam  user_id required The id of the user. Example: me
    * @queryParam  user_id required The id of the user. Example: me
    * @bodyParam  title string required The title of the post.
    * @bodyParam  body string required The content of the post.
    * @bodyParam  type string The type of post to create. Defaults to 'textophonious'.
    * @bodyParam  author_id int the ID of the author. Example: 2
    * @bodyParam  thumbnail image This is required if the post type is 'imagelicious'.
     */
    public function index()
    {
        return Bairro::get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validator = $request->validate([
                  'nome'=>'required|max:191',
              ]);

         if ($validator) {
             
            try {
                return Bairro::create($request->all());
                
            } catch (Exception $e) {
                return $e;
            }
             
         }else{
            return $validator;
         }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bairro  $bairro
     * @return \Illuminate\Http\Response
     */
    public function show(Bairro $bairro)
    {
        return $bairro;
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bairro  $bairro
     * @return \Illuminate\Http\Response
     */
    public function edit(Bairro $bairro)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bairro  $bairro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bairro $bairro)
    {
        
         $validator = $request->validate([
                  'nome'=>'required|max:191',
              ]);

         if ($validator) {
             
            try {
                $result = $bairro->update($request->all());

                if ($result) {
                    return $bairro;
                }else{
                    return response()->json(["message"=>'Erro ao atualizar bairro']);
                }
                
            } catch (Exception $e) {
                return $e;
            }
             
         }else{
            return $validator;
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bairro  $bairro
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bairro $bairro)
    {
        try {
             $result = $bairro->delete();
             if ($result) {
                return response()->json(['message'=>"Bairro removido com sucesso"]);
             }else{
                return response()->json(['message'=>"Erro removendo o bairro"]);
             }
        } catch (Exception $e) {
            return $e;
        }
        
    }
}
