<?php

namespace App\Http\Controllers;

use App\Benificiario;
use Illuminate\Http\Request;
use App\Http\Resources\Benificiario as BenificiarioResource;

class BenificiarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return BenificiarioResource::collection(Benificiario::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validator = $request->validate([
                  'ref'=>'required',
                  'nome'=>'required|max:191',
                  'numero_de_visita'=>'required',
                  'data_de_nascimento'=>'required',
                  'data_de_atendimento'=>'required',
                  'bairro_id'=>'required',
                  'sexo_id'=>'required',
                  'proviniencia_id'=>'required',
              ]);
         if ($validator) {
            try {
                $benificiario = Benificiario::create($request->all());
                Benificiario::find($benificiario->id)->contactos()->save(\App\Contacto::create(
                ['contacto'=>$request['contacto'],
                'nome_do_responsavel'=>$request['nome_do_responsavel'],
                'benificiario_id'=>$benificiario->id]
                ));
                return  new BenificiarioResource($benificiario);

            } catch (Exception $e) {
                return $e;
            }
             
         }else{
            return $validator;
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Benificiario  $benificiario
     * @return \Illuminate\Http\Response
     */
    public function show(Benificiario $benificiario)
    {
        return  new BenificiarioResource($benificiario);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Benificiario  $benificiario
     * @return \Illuminate\Http\Response
     */
    public function edit(Benificiario $benificiario)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Benificiario  $benificiario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Benificiario $benificiario)
    {
         $validator = $request->validate([
                  'ref'=>'required',
                  'nome'=>'required|max:191',
                  'numero_de_visita'=>'required',
                  'data_de_nascimento'=>'required',
                  'data_de_atendimento'=>'required',
                  'bairro_id'=>'required',
                  'sexo_id'=>'required',
                  'proviniencia_id'=>'required',
              ]);
         if ($validator) {
            try {
                $benificiario->update($request->all());
                Benificiario::find($benificiario->id)->contactos()->where(['benificiario_id'=>$benificiario->id])->update(
                ['contacto'=>$request['contacto'],
                'nome_do_responsavel'=>$request['nome_do_responsavel'],
                'benificiario_id'=>$benificiario->id]);
                return  new BenificiarioResource($benificiario);

            } catch (Exception $e) {
                return $e;
            }
             
         }else{
            return $validator;
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Benificiario  $benificiario
     * @return \Illuminate\Http\Response
     */
    public function destroy(Benificiario $benificiario)
    {

        try {
            Benificiario::find($benificiario->id)->contactos()->delete();
            if (Benificiario::find($benificiario->id)->benificiarioEncaminhado!=null) {
                Benificiario::find($benificiario->id)->benificiarioEncaminhado->servicoEncaminhados()->detach(); 
                Benificiario::find($benificiario->id)->benificiarioEncaminhado->documentos()->detach();
                Benificiario::find($benificiario->id)->benificiarioEncaminhado->delete();
            }
            $result = $benificiario->delete();
            
            if ($result) {
                 return response()->json(["message"=>"Beneficiário removido com sucesso"]);
            }else{
                 return response()->json(["message"=>"Erro ao remover beneficiário"]);
            }
           
            
        } catch (Exception $e) {
            return $e;
        }
        
    }
}
