<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proviniencia extends Model
{
    protected $fillable = [
        'nome',
    ];

     public function benificiarios()
    {
        return $this->hasMany('App\Benificiario');
    }
}
