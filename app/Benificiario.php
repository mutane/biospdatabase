<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Benificiario extends Model
{
    protected $fillable = [
        'nome','ref','numero_de_visita','data_de_nascimento','data_de_atendimento','provincia_id','bairro_id','sexo_id','objectivo_da_visita_id','proviniencia_id'
    ];

     public function bairro()
    {
        return $this->belongsTo('App\Bairro');
    }

     public function provincia()
    {
        return $this->belongsTo('App\Provincia');
    }

     public function proviniencia()
    {
        return $this->belongsTo('App\Proviniencia');
    }

     public function sexo()
    {
        return $this->belongsTo('App\Sexo');
    }

     public function objectivoDaVisita()
    {
        return $this->belongsTo('App\ObjectivoDaVisita');
    }

    public function contactos()
    {
        return $this->hasMany('App\Contacto');
    }

     public function benificiarioEncaminhado()
    {
        return $this->hasOne('App\BenificiarioEncaminhado');
    }

}
