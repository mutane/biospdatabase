<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provincia extends Model
{
     protected $fillable = [
        'nome',
    ];

     public function benificiarios()
    {
        return $this->hasMany('App\Benificiario');
    }
}
