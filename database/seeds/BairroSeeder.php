<?php

use Illuminate\Database\Seeder;
use App\Bairro;
class BairroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       factory(App\Bairro::class, 10)->create();
    }
}
