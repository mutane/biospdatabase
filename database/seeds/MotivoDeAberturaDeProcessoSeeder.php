<?php

use Illuminate\Database\Seeder;

class MotivoDeAberturaDeProcessoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\MotivoDeAberturaDeProcesso::class, 11)->create();
    }
}
