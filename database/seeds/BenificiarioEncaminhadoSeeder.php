<?php

use Illuminate\Database\Seeder;
use App\ServicoEncaminhado;

class BenificiarioEncaminhadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $bens =  factory(App\BenificiarioEncaminhado::class, 3)->create();

       foreach ($bens as $ben) {
             $ben->documentos()->attach(\App\Documento::all());
             $ben->servicoEncaminhados()->attach(\App\ServicoEncaminhado::all());
         }
      
    }
   // servicoEncaminhados
}
