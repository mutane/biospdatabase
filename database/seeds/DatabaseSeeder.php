<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UserSeeder::class);
         $this->call(BairroSeeder::class);
         $this->call(ContactoSeeder::class);
         $this->call(DocumentoSeeder::class);
         $this->call(MotivoDeAberturaDeProcessoSeeder::class);
         $this->call(ObjectivoDaVisitaSeeder::class);
         $this->call(ProvinciaSeeder::class);
         $this->call(ServicoEncaminhadoSeeder::class);
         $this->call(SexoSeeder::class);
         $this->call(ProvinienciaSeeder::class);
         $this->call(BenificiarioSeeder::class);
         $this->call(BenificiarioEncaminhadoSeeder::class);
         
    }
}
