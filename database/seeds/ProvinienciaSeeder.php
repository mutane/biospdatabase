<?php

use Illuminate\Database\Seeder;

class ProvinienciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Proviniencia::class, 6)->create();
    }
}
