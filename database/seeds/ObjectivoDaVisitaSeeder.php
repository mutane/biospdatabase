<?php

use Illuminate\Database\Seeder;

class ObjectivoDaVisitaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       factory(App\ObjectivoDaVisita::class, 5)->create();
    }
}
