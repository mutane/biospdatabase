<?php

use Illuminate\Database\Seeder;
use App\Sexo;
class SexoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       factory(App\Sexo::class, 2)->create();
    }
}
