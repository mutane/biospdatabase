<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->where('email', 'admin@gmail.com')->delete();

        DB::table('users')->insert([
            'name' => 'John Doe',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('admin@admin'),
            'remember_token'=>bcrypt('token'),
            'created_at'=>now(),
            'updated_at'=>now(),
            'email_verified_at'=>now()
        ]);

         factory(App\User::class, 1)->create();
    }
}
