<?php

use Illuminate\Database\Seeder;

class ServicoEncaminhadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\ServicoEncaminhado::class, 9)->create();
    }
}
