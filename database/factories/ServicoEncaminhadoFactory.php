<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ServicoEncaminhado;
use Faker\Generator as Faker;

$factory->define(ServicoEncaminhado::class, function (Faker $faker) {
    return [
        'nome' => $faker->name
    ];
});
