<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Benificiario;
use Faker\Generator as Faker;

$factory->define(Benificiario::class, function (Faker $faker) {
    return [
        'nome'=>$faker->name,'ref'=>$faker->uuid,'numero_de_visita'=>$faker->randomDigit,'data_de_nascimento' => $faker->date('y-m-d','2007'),'data_de_atendimento' => now(),'provincia_id'=>$faker->randomDigit,'bairro_id'=>$faker->randomDigit,'sexo_id'=>$faker->randomDigit,'objectivo_da_visita_id'=>$faker->randomDigit,'proviniencia_id' =>random_int(0, 7)
    ];
});
