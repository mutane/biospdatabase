<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ObjectivoDaVisita;
use Faker\Generator as Faker;

$factory->define(ObjectivoDaVisita::class, function (Faker $faker) {
    return [
        'nome' => $faker->name
    ];
});
