<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Contacto;
use Faker\Generator as Faker;

$factory->define(Contacto::class, function (Faker $faker) {
    return [
       'contacto'=> $faker->phoneNumber,'nome_do_responsavel' => $faker->name,'benificiario_id'=>$faker->randomDigit
    ];
});
