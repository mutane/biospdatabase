<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\MotivoDeAberturaDeProcesso;
use Faker\Generator as Faker;

$factory->define(MotivoDeAberturaDeProcesso::class, function (Faker $faker) {
    return [
        'nome' => $faker->name
    ];
});
