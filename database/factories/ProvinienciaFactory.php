<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Proviniencia;
use Faker\Generator as Faker;

$factory->define(Proviniencia::class, function (Faker $faker) {
    return [
         'nome' => $faker->name
    ];
});
