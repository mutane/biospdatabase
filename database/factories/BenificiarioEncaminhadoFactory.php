<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\BenificiarioEncaminhado;
use Faker\Generator as Faker;

$factory->define(BenificiarioEncaminhado::class, function (Faker $faker) {
    return [
         'objecto_da_visita' =>$faker->titleMale,'data_que_foi_recebido' => now(),'problema_resolvido' => $faker->boolean,'benificiario_id'=>$faker->randomDigit,'motivo_de_abertura_de_processo_id'=>$faker->randomDigit
    ];
});
