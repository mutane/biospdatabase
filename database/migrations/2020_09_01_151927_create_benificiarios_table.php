<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBenificiariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('benificiarios', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->string('ref');
            $table->integer('numero_de_visita');
            $table->date('data_de_nascimento');
            $table->date('data_de_atendimento');
            $table->foreignId('provincia_id')->nullable();
            $table->foreignId('bairro_id');
            $table->foreignId('sexo_id');
            $table->foreignId('proviniencia_id');
            $table->foreignId('objectivo_da_visita_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('benificiarios');
    }
}
