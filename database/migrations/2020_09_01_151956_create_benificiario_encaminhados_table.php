<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBenificiarioEncaminhadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('benificiario_encaminhados', function (Blueprint $table) {
            $table->id();
            $table->text('objecto_da_visita')->nullable();
            $table->boolean('necessita_de_acompanhamento_domicialiar')->default(0);
            $table->date('data_que_foi_recebido');
            $table->boolean('problema_resolvido')->default(0);
            $table->foreignId('benificiario_id')->nullable();
            $table->foreignId('motivo_de_abertura_de_processo_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('benificiario_encaminhados');
    }
}
