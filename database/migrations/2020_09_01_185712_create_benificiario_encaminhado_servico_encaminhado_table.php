<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBenificiarioEncaminhadoServicoEncaminhadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('benificiario_encaminhado_servico_encaminhado', function (Blueprint $table) {
            $table->id();
            $table->foreignId('benificiario_encaminhado_id');
            $table->foreignId('servico_encaminhado_id');
            $table->string('especificar')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('benificiario_encaminhado_servico_encaminhado');
    }
}
